#!/bin/bash
#cp bashrc ~/.bashrc
cp gitconfig ~/.gitconfig
cp profile ~/.profile
#cp vimrc ~/.vimrc
cp tmux.conf ~/.tmux.conf
cp gdbinit ~/.gdbinit
cp cuda-gdbinit ~/.cuda-gdbinit
cp gdbinit.d ~/.gdbinit.d -rf

