# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022
# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi

export LC_ALL=en_US.UTF-8
#export TERM=xterm-256color
#export TERM=xterm-256color
# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"
export CFLAGS=-I/usr/local/cuda/include
export LDFLAGS=-L/usr/local/cuda/lib64
export CUDA_PATH=/usr/local/cuda
export CUDA_ROOT=/usr/local/cuda
export CUDA_HOME=/usr/local/cuda
export PATH=$PATH:$HOME/miniconda3/bin
export PATH="$CUDA_PATH/bin:$PATH"
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/speechAI/kaldi/tools/openfst/lib:$LD_LIBRARY_PATH

export KALDI_ROOT=$HOME/speechAI/kaldi

export PATH=$PWD/utils/:$KALDI_ROOT/tools/openfst/bin:$PWD:$PATH
. $KALDI_ROOT/tools/config/common_path.sh

. $KALDI_ROOT/tools/env.sh
